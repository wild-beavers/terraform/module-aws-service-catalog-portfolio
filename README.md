# module-aws-service-catalog-portfolio

Terraform module that deploys a service-catalog portfolio.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.44 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 3.44 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_servicecatalog_portfolio.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/servicecatalog_portfolio) | resource |
| [aws_servicecatalog_portfolio_share.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/servicecatalog_portfolio_share) | resource |
| [aws_servicecatalog_principal_portfolio_association.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/servicecatalog_principal_portfolio_association) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_description"></a> [description](#input\_description) | Description of the portfolio. | `string` | `null` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of the portfolio. | `string` | `"portfolio"` | no |
| <a name="input_portfolio_principals"></a> [portfolio\_principals](#input\_portfolio\_principals) | Map of principals associated to the portfolio, following this structure:<br>  {<br>    foo = {<br>      principal\_arn   = string<br>      accept\_language = optional(string)<br>      principal\_type  = optional(string)<br>    }<br>  } | `map(map(string))` | `{}` | no |
| <a name="input_portfolio_shares"></a> [portfolio\_shares](#input\_portfolio\_shares) | Map of shares for the portfolio following:<br>{<br>  foo = {<br>    principal\_id        = string<br>    type                = string<br>    accept\_language     = optional(string)<br>    share\_tag\_options   = optional(bool)<br>    wait\_for\_acceptance = optional(bool)<br><br>  }<br>} | `map(map(string))` | `{}` | no |
| <a name="input_provider_name"></a> [provider\_name](#input\_provider\_name) | Name of the provider of the portfolio. | `string` | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Map of tags that will be applied on all the resources. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_id"></a> [id](#output\_id) | ID of the portfolio. |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
