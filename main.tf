#####
# Locals
#####

locals {
  tags = {
    managed-by = "terraform"
  }
}

######
# Portfolio
######

resource "aws_servicecatalog_portfolio" "this" {
  name          = var.name
  description   = var.description
  provider_name = var.provider_name
  tags          = merge(local.tags, var.tags)
}

#####
# Portfolio Share
#####

resource "aws_servicecatalog_portfolio_share" "this" {
  for_each = var.portfolio_shares

  portfolio_id        = aws_servicecatalog_portfolio.this.id
  principal_id        = lookup(each.value, "principal_id")
  type                = lookup(each.value, "type")
  accept_language     = lookup(each.value, "accept_language", null)
  share_tag_options   = lookup(each.value, "share_tag_options", null)
  wait_for_acceptance = lookup(each.value, "wait_for_acceptance", null)
}

#####
# Portfolio Principal Association
#####

resource "aws_servicecatalog_principal_portfolio_association" "this" {
  for_each = var.portfolio_principals

  portfolio_id    = aws_servicecatalog_portfolio.this.id
  principal_arn   = lookup(each.value, "principal_arn")
  accept_language = lookup(each.value, "accept_language", null)
  principal_type  = lookup(each.value, "principal_type", null)
}
