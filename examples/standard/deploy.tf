module "this" {
  source = "../../"

  name          = "my-portfolio"
  description   = "test portfolio"
  provider_name = "wild-beavers"
  tags = {
    purpose = "testing"
  }
}
